import { browser, ElementFinder, ProtractorExpectedConditions } from "protractor";
import { protractor } from "protractor/built/ptor";
var chai = require('chai');  
var expect = chai.expect;

export class protractor_base {

    private ec: ProtractorExpectedConditions = browser.ExpectedConditions;
    private timeOut = 40000;

    /**
     * @description This function for click action
     * @param element - The element on whick click action to be performed
     */
    public async click(element: ElementFinder) {
        await browser.wait(this.ec.elementToBeClickable(element), this.timeOut,
            "Element is not clickable fail click action");
        await element.click();
    }
	
    /**
     * @description This function to append the text 
     * @param element locator for element
     * @param sentData Data to be typed
     */
    public async type(element: ElementFinder, sentData: string) {
        await this.visibilityOf(element);
        await element.sendKeys(sentData);
    }

    /**
    * @description This function will clear the existing value and then type the data
    * @param element Pass the element locator
    * @param sentData Data to be typed on the element
    */
    public async clearAndType(element: ElementFinder, sentData: string) {
        await this.visibilityOf(element);
        await element.clear()
        await element.sendKeys(sentData);
    }

    /**
    * @description This function compare text
    * @param expectedText Data to be compare on the title
    */
    public async assertTitle(expectedText: string) {
        await browser.wait(this.ec.titleIs(expectedText), 50000,"Title is not visible");
        let titleText = await browser.getTitle()
        expect(titleText.trim()).to.equal(expectedText);
    }

        /**
    * @description This function compare title text
    * @param element Pass the element locator
    * @param sentData Data to be typed on the element
    */
   public async assertText(element: ElementFinder, expectedText: string) {
    await this.visibilityOf(element);
    let actualText = await element.getText();
    expect(actualText.trim()).to.equal(expectedText);
    }   

    /**
    * @description This function to check elementis visible 
    * @param element Pass the element locator
    */
    private async visibilityOf(element: ElementFinder) {
        await browser.wait(this.ec.visibilityOf(element), this.timeOut,
            "Element is not visible");
    }

    /**
    * @description This function to check elementis inVisibilityOf element 
    * @param element Pass the element locator
    */
    protected async inVisibilityOf(element: ElementFinder) {
        await browser.wait(this.ec.invisibilityOf(element), this.timeOut,
            "Element is still visible");
    }
	
	/**
    * @description This function to check element visible
    * @param element Pass the element locator
    */
    public async assertTrue(element: ElementFinder) {
        await this.visibilityOf(element);
        expect(await element.isDisplayed()).be.true;
    }

	/**
    * @description This function to check element not visible
    * @param element Pass the element locator
    */
    public async assertFalse(element: ElementFinder) {
        await this.visibilityOf(element);
        expect(await element.isDisplayed()).be.false;
    }

	/**
    * @description This function to accept Alert
    */
    public async acceptAlert() {
        await browser.wait(this.ec.alertIsPresent(), this.timeOut, "Alert is not present");
        await (await browser.switchTo().alert()).accept();
    }

	/**
    * @description This function to accept Dismiss
    */
    public async dismissAlert() {
        await this.waitForAlert();
        await (await browser.switchTo().alert()).dismiss();
    }

	/**
    * @description This function wait for alert
    */
    private async waitForAlert() {
        await browser.wait(this.ec.alertIsPresent(), this.timeOut, "Alert is not present");
    }

	/**
    * @description This function type in alert
	* @param data text to pass to alert
    */
    public async tyepInAlert(data: string) {
        await this.waitForAlert();
        await (await browser.switchTo().alert()).sendKeys(data);
    }
	
	/**
    * @description This function get alert text
	* @return alert text pass to alert
    */
    public async getTextFromAlert(): Promise<string> {
        await this.waitForAlert();
        let alertText = await (await browser.switchTo().alert()).getText();
        return alertText;
    }

	/**
    * @description This function to switch control to frame
	* @param frameNumber frame number
    */
    public async switchToFrame(frameNumber: number) {
        await browser.switchTo().frame(frameNumber);
    }

	/**
    * @description This function to enter text and TAB key
	* @param element Pass the element locator	
	* @param testData text to write
    */
    public async typeAndTab(element: ElementFinder, testData: string) {
        await this.visibilityOf(element);
        await element.clear();
        await element.sendKeys(testData, protractor.Key.TAB)
    }

	/**
    * @description This function to enter text and ENTER key for window OS
	* @param element Pass the element locator	
	* @param testData text to write
    */
    public async typeAndEnter(element: ElementFinder, testData: string) {
        let capabilities = await browser.getCapabilities()
        let platform = capabilities.get('platform');
        await this.visibilityOf(element);
        await element.clear();
        await element.sendKeys(testData, protractor.Key.ENTER)
    }

	/**
    * @description This function to mouse Hover And Click on element
	* @param element Pass the element locator	
    */
    public async mouseHoverAndClick(element: ElementFinder) {
        await browser.actions()
            .mouseMove(await element.getWebElement())
            .click()
            .perform();

    }
	
	/**
    * @description This function to move a mouse to on element
	* @param element Pass the element locator	
    */
    public async moveToElement(element: ElementFinder) {
        await browser.actions()
            .mouseMove(await element.getWebElement())
            .perform();
    }

}