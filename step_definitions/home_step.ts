import { Given, setDefaultTimeout, Then, When } from "cucumber";
import { browser } from "protractor";
import { homepage } from "../page/homepage";
import { weather } from "../page/weather";

const HomePage = new homepage()
const Weather = new weather()

setDefaultTimeout(90 * 1000);

Given('I am on NDTV news website home', async function () {
   await browser.manage().timeouts().pageLoadTimeout(100000); 
   await browser.manage().timeouts().setScriptTimeout(60000); 
   await browser.waitForAngularEnabled(false); 

   await browser.driver.get("https://www.ndtv.com/");
   
   console.log(await browser.getTitle());
   await HomePage.closeAlertPopup();
   await HomePage.varify_logo();
});

When('I wish to see weather report', async function () {
    await HomePage.click_Menu_Expand();
    await HomePage.click_Menu_Weather();
});

Then('mejor city weather information is displayed', async function () {
    await Weather.varify_Weather_home();
});


