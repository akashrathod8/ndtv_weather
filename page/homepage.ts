  
import { element, by } from "protractor";
import { commonFunctions } from "../projectBase/common/commonFunctions";

export class homepage extends commonFunctions {
    
    private logo =  element(by.className("ndtvlogo"))
    private menu_Expand = element(by.id("h_sub_menu"))
    private menu_Weather = element(by.linkText('WEATHER'))
    private alertPopupNoThanks = element(by.css("a[class='notnow']"))
    
    private NDTV_title = "Get Latest News, India News, Breaking News, Today's News - NDTV.com" 
        
    async varify_Logo() {
        await this.assertTrue(this.logo);
    }
    
    async varify_Title() {
        await this.assertTitle(this.NDTV_title);
    }

    async click_Menu_Expand() {
        await this.click(this.menu_Expand);
    }

    async click_Menu_Weather() {
        await this.click(this.menu_Weather);
    }
    
    async closeAlertPopup(){
        await this.click(this.alertPopupNoThanks);
    }
}
