import { element, by } from "protractor";
import { commonFunctions } from "../projectBase/common/commonFunctions";

export class weather extends commonFunctions {
    
    private weatherpageText = element(by.cssContainingText('div','Current weather conditions in your city.'));
    
    async varify_Weather_home(){
        await this.assertTrue(this.weatherpageText);
    }
}