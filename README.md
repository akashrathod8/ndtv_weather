This is BDD based modular driven framework, written using protractor cucumber. 
created by - Akash Rathod  
email - akash.rathod8@gmail.com

Setup
required software installation
1. node 10.16.9
2. java 1.8.0_251
3. VS code editor (or any other editor) 

installation commands
npm i protractor
npm i cucumber@6.0.5
npm i protractor-cucumber-framework
npm i (will install all dependency from package.json)
  
project Details -
1. TypeScript Project  
2. Page Object Model
3. BDD cucumber framework

Features need to implement -
1. In-step exception handling
2. In-step logger
3. Common utlis
4. API lib

configuration file is at below path configuration -> conf.protractor.js

run project using 
1. run.bat
2. protractor configuration\conf.protractor.js

