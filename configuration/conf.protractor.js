/*
protractor JS configuration file to run cucumber feature. 
**/

exports.config = {
    //seleniumAddress: 'http://localhost:4444/wd/hub',
    directConnect: true,
    baseUrl: 'https://www.ndtv.com/',
    capabilities: {
        browserName:'chrome'
    },
    framework: 'custom',  // "custom" cucumber framework.
    frameworkPath: require.resolve('protractor-cucumber-framework'),
    specs: [
      '../features/*.feature'     // Specs here are the cucumber feature files
    ],
    SELENIUM_PROMISE_MANAGER: false,
    cucumberOpts: {
      require: ['../step_definitions/*.ts'],
      tags: [],
      format: ['json:reports/results.json']
    },
   onPrepare: function () {
      require("ts-node").register({
        project: require("path").join(__dirname, "../tsconfig.json"), // Relative path of tsconfig.json file
      });
      browser.manage().window().maximize(); 
      browser.ignoreSynchronization = true;
      //browser.waitForAngularEnabled(false); 

    },
    onComplete: function(){
      var reporter = require('cucumber-html-reporter');
 
      var options = {
        theme: 'bootstrap',
        jsonFile: 'reports/results.json',
        output: 'reports/cucumber_report.html',
        reportSuiteAsScenarios: true,
        scenarioTimestamp: true,
        launchReport: true,
        metadata: {
            "App Version":"0.3.2",
            "Test Environment": "STAGING",
            "Browser": "Chrome  54.0.2840.98",
            "Platform": "Windows 10",
            "Parallel": "Scenarios",
            "Executed": "Remote"
        }
    };
 
    reporter.generate(options);
    }

  };